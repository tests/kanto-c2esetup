# Notices for Eclipse Kanto

This content is produced and maintained by the Eclipse Kanto project.

* Project home: https://eclipse.org/kanto

## Trademarks

Eclipse Kanto, and the Eclipse Kanto Logo are trademarks of the Eclipse Foundation.
Eclipse, and the Eclipse Logo are registered trademarks of the Eclipse Foundation.

## Copyright

All content is the property of the respective authors or their employers.
For more information regarding authorship of content, please consult the
listed source code repository logs.

## Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License 2.0 which is available at
https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0 which
is available at https://www.apache.org/licenses/LICENSE-2.0.

SPDX-License-Identifier: EPL-2.0 OR Apache-2.0

## Source Code

The project maintains all source code repositories in the following GitHub organization:

* https://github.com/eclipse-kanto

## Third-party Content

This project makes use of the follow third party projects.

gohugoio/hugo (0.89.4)

* License: Apache License 2.0
* Project: https://github.com/gohugoio/hugo
* Source:  https://github.com/gohugoio/hugo/releases/tag/v0.89.4

google/docsy

* License: Apache License 2.0
* Project: https://github.com/google/docsy
* Source:  https://github.com/google/docsy

PrismJS/prism (1.25.0)

* License: MIT License
* Project: https://github.com/PrismJS/prism
* Source:  https://github.com/PrismJS/prism/releases/tag/v1.25.0

apache/qpid-proton (0.36.0)

* License: Apache License 2.0
* Project: https://github.com/apache/qpid-proton
* Source:  https://github.com/apache/qpid-proton/tree/0.36.0

eclipse/paho.mqtt.golang (1.4.1)

* License: Eclipse Distribution License v1.0
* Project: https://github.com/eclipse/paho.mqtt.golang
* Source:  https://github.com/eclipse/paho.mqtt.golang/releases/tag/v1.4.1

google/uuid (1.3.0)

* License: BSD 3-Clause "New" or "Revised" License
* Project: https://github.com/google/uuid
* Source:  https://github.com/google/uuid/releases/tag/v1.3.0

gorilla/websocket (1.4.2)

* License: BSD 2-Clause "Simplified" License
* Project: https://github.com/gorilla/websocket
* Source:  https://github.com/gorilla/websocket/releases/tag/v1.4.2

golang.org/x/crypto (v0.0.0-20190308221718-c2843e01d9a2)

* License: BSD 3-Clause "New" or "Revised" License
* Project: https://github.com/golang/net
* Source:  https://github.com/golang/crypto/tree/c2843e01d9a2bc60bb26ad24e09734fdc2d9ec58

golang.org/x/net (v0.0.0-20200425230154-ff2c4b7c35a0)

* License: BSD 3-Clause "New" or "Revised" License
* Project: https://github.com/golang/net
* Source:  https://github.com/golang/net/tree/ff2c4b7c35a07b0c1e90ce72aa7bfe41bb66a3cb

golang.org/x/sync (v0.0.0-20210220032951-036812b2e83c)

* License: BSD 3-Clause "New" or "Revised" License
* Project: https://github.com/golang/sync
* Source:  https://github.com/golang/sync/tree/036812b2e83c0ddf193dd5a34e034151da389d09

golang.org/x/sys (v0.0.0-20190215142949-d0b11bdaac8a)

* License: BSD 3-Clause "New" or "Revised" License
* Project: https://github.com/golang/sys
* Source:  https://github.com/golang/sys/tree/d0b11bdaac8adb652bff00e49bcacf992835621a

golang.org/x/text (v0.3.0)

* License: BSD 3-Clause "New" or "Revised" License
* Project: https://github.com/golang/text
* Source:  https://github.com/golang/text/releases/tag/v0.3.0

## Cryptography

Content may contain encryption software. The country in which you are currently
may have restrictions on the import, possession, and use, and/or re-export to
another country, of encryption software. BEFORE using any encryption software,
please check the country's laws, regulations and policies concerning the import,
possession, or use, and re-export of encryption software, to see if this is
permitted.
